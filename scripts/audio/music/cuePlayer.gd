"""
Musical Cue Player for Faehna
Copyleft 2023 Arris Kathery
"""
extends AudioServer

var cues = new Dictionary();

func _loadCuesDat(path = "res://data/json/cues.json"):
  if path == "":
    printerr("no path given");
    return ERR_PRINTER_ON_FIRE;
  
  print_debug("loading cue data from {0}...".format(path))
  if !FileAccess.file_exists(path):
    printerr("does not exist");
    return ERR_FILE_NOT_FOUND;
  
  var toPush = JSON.parse_string(FileAccess.get_file_as_string(path));
  print_debug("loaded and parsed cue file");
  
  if toPush:cues = toPush;
  else: printerr("no data");return ERR_INVALID_DATA;
  return OK;
  
  
