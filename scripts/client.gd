extends Node
@export_node_path("TextEdit")
var ip_addr_path : NodePath;
@export_node_path("TextEdit")
var port_path : NodePath;
@export_node_path("TextEdit")
var message_path : NodePath;
@export_node_path("Button")
var trigger_path : NodePath;

var ip_addr : TextEdit;
var port : TextEdit;
var message : TextEdit;
var trigger : Button;

var connection : PacketPeerUDP = null#StreamPeer = null;

func _ready():
  ip_addr = get_node(ip_addr_path);
  port = get_node(port_path);
  message = get_node(message_path);
  trigger = get_node(trigger_path);
  trigger.connect("pressed",triggered)
  return;

func triggered():
  if connection == null:
    connection = PacketPeerUDP.new()#StreamPeerTCP.new()
    
    print(ip_addr.text + str(int(port.text)));
    var err = connection.connect_to_host("127.0.0.1",65000)
    #var err = connection.connect_to_host(ip_addr.text,int(port.text))
    #connection.set_no_delay(true)
    print(err)
    trigger.text = "send";
    
  else:
    #var err = connection.put_packet(PackedByteArray(message.text.to_utf8_buffer())) 
    #connection.put_data(message.text.to_utf8_buffer());
    connection.put_packet(message.text.to_utf8_buffer())
  return;
